
let SCRAPDATA = function(url,done) {
    const puppeteer = require('puppeteer'); // v 1.1.0
    const $ = require('cheerio');
    const passJetbrains = 'R:3.5m3JG&2Sm.L<';
    const fs = require('fs');
    const translate = require('translate');

//const url = 'https://www.tripadvisor.com.gr/Hotel_Review-g189400-d233051-Reviews-The_Stanley-Athens_Attica.html';
//https://www.tripadvisor.com.gr/Hotel_Review-g189400-d233051-Reviews-The_Stanley-Athens_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g663107-d665370-Reviews-Yianna_Hotel-Angistri_Saronic_Gulf_Islands_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g3464181-d1820924-Reviews-Hotel_Agistri-Skala_Angistri_Saronic_Gulf_Islands_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g189400-d677259-Reviews-Hotel_Ariston-Athens_Attica.html
//'https://www.tripadvisor.com.gr/Hotel_Review-g189400-d1510627-Reviews-Athens_Lotus_Hotel-Athens_Attica.html';

    let reviewsArrExc = [];
    let reviewsArrCtn = [];
    let REVIEWS = [];
    let PAGECOUNTSexists = 0;
    let pageCount = 6;
    let clicked = true;

    //Trip Advisor Selectors
    let community_content_Card_reg = /hotels-community-tab-common-Card__card--(\w){5}/g;
    let name_reg = /social-member-event-MemberEventOnObjectBlock__member_event_block--(\w){5}/g;
    let hotels_hotel_review_req = /hotels-review-list-parts-ExpandableReview__reviewText--(\w){5}/g;
    let hotels_hotel_Title_req = /hotels-review-list-parts-ReviewTitle__reviewTitle--(\w){5}/g;
    let date_req = /social-member-event-MemberEventOnObjectBlock__event_type--(\w){5}/g;
    let revdate_req = /hotels-review-list-parts-EventDate__event_date--(\w){5}/g;//Hmeromhnia Diamonhs
    let hometown_req =  /social-member-common-MemberHometown__hometown--(\w){5}/g;

    let CONT_CONTENT = '#taplc_resp_hr_ad_wrapper_lower_block_obj_first_0';

    let pagination_reg = /hotels-community-pagination-card-PaginationCard__wrapper--(\w){5}/g;

    let content_revie_list_req = /hotels-review-list-parts-ExpandableReview__ctaLine--(\w){5}/g;

    let ReviewCounterLang = 0;

    function scrapCheeriosExcerpt(context,lang) {

        let hotels_community_content_Card = $(CONT_CONTENT, context).html().match( community_content_Card_reg )
        let hotels_name_Member =  $(CONT_CONTENT, context).html().match(  name_reg  )
        let hotels_review =  $(CONT_CONTENT, context).html().match(  hotels_hotel_review_req  )
        let hotels_hotel_Title =  $(CONT_CONTENT, context).html().match(  hotels_hotel_Title_req  )
        let hotels_date =  $(CONT_CONTENT, context).html().match(  date_req  )
        let hotels_revdate = $(CONT_CONTENT, context).html().match( revdate_req   )
        let hotels_hometown = $(CONT_CONTENT, context).html().match( hometown_req )


        console.log( " hotels_community_content_Card 1121 " + hotels_community_content_Card)

        if( hotels_name_Member !== null &&  hotels_hotel_Title !== null){

            $( "." + hotels_community_content_Card[0] , context).each(function () {

                let review = {
                    name: "",
                    heading: "",
                    content: "",
                    excerpt: ""
                }

                let hometown = " ";

                if( !(hotels_hometown == null) ){
                    hometown = $(this).find("." + hotels_hometown[0]).text()
                }

                let name = $(this).find("." + hotels_name_Member[0]).text()
                let reviewText = $(this).find("q." + hotels_name_Member[0]).text()
                let heading = $(this).find("." + hotels_hotel_Title[0]).text()
                let link = $(this).find("." + hotels_hotel_Title[0]).attr('href')
                let date = $(this).find("." + hotels_date[0]).text()
                let revdate = $(this).find("." + hotels_revdate[0]).text()


                review.name = name;
                review.heading = heading;
                review.excerpt = reviewText;
                review.link = link;
                review.lang = lang;
                review.date = date;
                review.revdate = revdate;
                review.hometown = hometown;

                reviewsArrExc.push(review)

            });

        }

    }

    function scrapCheeriosContent(context,lang) {

        let hotels_community_content_Card = $(CONT_CONTENT, context).html().match(  community_content_Card_reg  )
        let hotels_name_Member =  $(CONT_CONTENT, context).html().match(  name_reg  )
        let hotels_review =  $(CONT_CONTENT, context).html().match(  hotels_hotel_review_req  )
        let hotels_hotel_Title =  $(CONT_CONTENT, context).html().match(  hotels_hotel_Title_req  )
        let hotels_date =  $(CONT_CONTENT, context).html().match(  date_req  )
        let hotels_revdate = $(CONT_CONTENT, context).html().match( revdate_req   )
        let hotels_hometown = $(CONT_CONTENT, context).html().match( hometown_req )

        console.log( " hotels_community_content_Card " + hotels_community_content_Card)
        console.log( "hotels_name_Member " + hotels_name_Member)
        console.log( "  hotels_review " +  hotels_review)
        console.log( " hotels_hotel_Title " + hotels_hotel_Title)
        console.log( "  hotels_date " +  hotels_date)
        console.log( " hotels_revdate  " + hotels_revdate )
        console.log( " hotels_hometown " + hotels_hometown)

        if(hotels_name_Member !== null && hotels_review !== null){
            $( "." + hotels_community_content_Card[0] , context).each(function () {

                let review = {
                    name: "",
                    heading: "",
                    content: "",
                    excerpt: ""
                }

                let revdate

                let name = $(this).find("." + hotels_name_Member[0]).text()
                let reviewText = $(this).find("q." + hotels_review[0]).text()
                let heading = $(this).find("." + hotels_hotel_Title[0]).text()
                let link = $(this).find("." + hotels_hotel_Title[0]).attr('href')
                let date = $(this).find("." + hotels_date[0]).text()

                if (hotels_revdate){
                    revdate = $(this).find("." + hotels_revdate[0]).text()
                }

                let dd = $(this).find("q." + hotels_review[0]).length

                let hometown = " ";

                // if( !( hotels_name_Member == null) ){
                //     name = $(this).find("." + hotels_name_Member[0]).text()
                // }

                if( !(hotels_hometown == null) ){
                    hometown = $(this).find("." + hotels_hometown[0]).text()
                }

                if (dd > 0) {
                    review.name = name;
                    review.heading = heading;
                    review.content = reviewText;
                    review.link = link;
                    review.lang = lang;
                    review.date = date;
                    review.revdate = revdate;
                    review.hometown = hometown;

                    reviewsArrCtn.push(review)
                }


            });
        }

    }

    async function init(page, scrapCheeriosExcerpt, scrapCheeriosContent, Lang , lang) {

        reviewsArrExc = [];
        reviewsArrCtn = [];
        REVIEWS = [];
        PAGECOUNTSexists = 0;
        pageCount = 6;

        await page.evaluate(() => {
            scroll(0, 99999)
        });

        await page.waitFor(5000);

        let bodyHTMLpages = await page.evaluate(() => document.body.innerHTML);

        let hotels_community_content_Card

        console.log("awsawawwaw111222")

        console.log('pagination_reg')
        if($( CONT_CONTENT , bodyHTMLpages ).html().match( pagination_reg ) !== null){
            hotels_community_content_Card = $( CONT_CONTENT , bodyHTMLpages ).html().match( pagination_reg )

        }

        console.log("awsawawwaw")
        console.log(hotels_community_content_Card)
        if(hotels_community_content_Card !== undefined){

            if ($("." + hotels_community_content_Card[0] , bodyHTMLpages).first().length) {

                PAGECOUNTSexists = $("." + hotels_community_content_Card[0] , bodyHTMLpages).first().find('.pageNum').length

                console.log("sasas " + PAGECOUNTSexists)
            }
        }

        console.log("tosa page counts uparxoun " + PAGECOUNTSexists)
        if (PAGECOUNTSexists <= pageCount) {
            if (PAGECOUNTSexists === 0) {
                pageCount = 1
            } else {
                pageCount = PAGECOUNTSexists
            }
        }
        console.log("tosa page counts uparxoun s " + pageCount)
        for (let i = 0; i < pageCount; i++) {

            let bodyHTMLex = await page.evaluate(() => document.body.innerHTML);

            if ($('#autoTranslateNo', bodyHTMLex).length && clicked) {

                console.log("ds")
                await page.$$eval('#autoTranslateNo', function (elements) {
                    elements[0].click()
                });
                clicked = false;

            }

            console.log($('.next', bodyHTMLex).first().not('.disabled').length)
            if (clicked && $('.next', bodyHTMLex).first().not('.disabled').length ) {

                console.log("Ok falarw egw??>>")
                await page.$$eval('.ui_pagination a.nav.next', function (elements) {
                    console.log("Ok falarw egw??>>")
                    elements[0].click()
                });

            }

            await page.waitFor(2000);
            console.log("EPOMONI SELIDA >>>")
        }

        console.log(clicked)

        console.log(pageCount)
        for (let i = 0; i < pageCount; i++) {

            let bodyHTMLex = await page.evaluate(() => document.body.innerHTML);

            let content_revie_list = $( CONT_CONTENT , bodyHTMLex ).html().match( content_revie_list_req )

            // console.log(content_revie_list)

            if( content_revie_list !== null){
                await page.$$eval("." + content_revie_list[0] , function (elements) {

                    elements[0].click()

                });
            }

            await page.waitFor(200);

            let bodyHTMLCtn = await page.evaluate(() => document.body.innerHTML);

            scrapCheeriosContent(bodyHTMLCtn, lang)

            let bodyHTMLexButtonPrev = await page.evaluate(() => document.body.innerHTML);

            if (clicked && $('a.previous', bodyHTMLexButtonPrev).not('.disabled').length) {

                await page.$$eval('a.previous', function (elements) {
                    elements[0].click()
                });

                await page.waitFor(2000);

            }else{
                console.log("den mpika pote")
            }

            let bodyHTMLexButtonNext = await page.evaluate(() => document.body.innerHTML);

            if (!clicked && $('.next', bodyHTMLexButtonNext).length) {

                await page.$$eval('.next', function (elements) {
                    elements[0].click()
                });
                await page.waitFor(2000);
                console.log("EPOMONI SELIDA >>")
            }

        }

        let bodyHTMLex = await page.evaluate(() => document.body.innerHTML);

        console.log($('.hotels-hotel-review-community-content-review-list-parts-ExpandableReview__cta--3_zOW', bodyHTMLex).length)

        let content_revie_list = $( CONT_CONTENT , bodyHTMLex ).html().match( content_revie_list_req )

        if( content_revie_list !== null){
            await page.$$eval("." + content_revie_list[0] , function (elements) {

                console.log("GIA NA DOUME")
                elements[0].click()
                console.log("GIA NA DOUME1")

            });
        }

        await page.waitFor(200);

        let bodyHTMLCtn = await page.evaluate(() => document.body.innerHTML);

        scrapCheeriosContent(bodyHTMLCtn, lang)

        // let counter = 0;
        // for (let i = 0; i < reviewsArrExc.length; i++) {
        //
        //     for (let y = 0; y < reviewsArrCtn.length; y++) {
        //
        //         if (reviewsArrExc[i].name === reviewsArrCtn[y].name && reviewsArrExc[i].heading === reviewsArrCtn[y].heading) {
        //
        //
        //             let rev = {
        //                 name: reviewsArrCtn[y].name,
        //                 heading: reviewsArrCtn[y].heading,
        //                 content: reviewsArrCtn[y].content,
        //                 excerpt: reviewsArrExc[y].excerpt,
        //                 lang: reviewsArrCtn[y].lang,
        //                 link: reviewsArrCtn[y].link,
        //                 date: reviewsArrCtn[y].date,
        //                 revdate: reviewsArrCtn[y].revdate,
        //                 // hometown: fooa
        //             }
        //
        //
        //             // translate.from = 'el';
        //             // let fooa = await translate( reviewsArrExc[y].hometown , { to: 'en', engine: 'google', key: 'AIzaSyCP6JXuY_k7qGsoSYDZm76m4FFpYv8Q-8Y' }).then(text => {
        //             //     console.log("------")
        //             //     console.log(text);  // Hola mundo
        //             //
        //             //     rev.hometown = text
        //             //     console.log("------")
        //             // });
        //             //
        //             // let foob = await translate( reviewsArrExc[y].date , { to: 'en', engine: 'google', key: 'AIzaSyCP6JXuY_k7qGsoSYDZm76m4FFpYv8Q-8Y' }).then(text => {
        //             //     console.log("------")
        //             //     console.log(text);  // Hola mundo
        //             //
        //             //     rev.date = text
        //             //     console.log("------")
        //             // });
        //             //
        //             // let fooc = await translate( reviewsArrExc[y].revdate , { to: 'en', engine: 'google', key: 'AIzaSyCP6JXuY_k7qGsoSYDZm76m4FFpYv8Q-8Y' }).then(text => {
        //             //     console.log("------")
        //             //     console.log(text);  // Hola mundo
        //             //
        //             //     rev.revdate = text
        //             //     console.log("------")
        //             // });
        //             //
        //             //
        //             // console.log(reviewsArrExc[y].hometown)
        //             // // console.log("to en")
        //             // // console.log(fooa)
        //
        //
        //             console.log("to vrikame " + counter)
        //             REVIEWS.push(rev)
        //             counter++
        //         }
        //
        //     }

        //}

        //array.reduce(function(total, currentValue, currentIndex, arr), initialValue)

        // let ds = REVIEWS.filter((thing, index, self) =>
        //     index === self.findIndex((t) => (
        //         t.heading === thing.heading && t.name === thing.name
        //     ))
        //     return
        // )

        console.log("LANG == " + ReviewCounterLang)
        let translateFirst = [];
        for( let i =0; i < reviewsArrCtn.length; i++ ){
            let rev = {
                name: reviewsArrCtn[i].name,
                heading: reviewsArrCtn[i].heading,
                content: reviewsArrCtn[i].content,
                excerpt: reviewsArrCtn[i].excerpt,
                lang: reviewsArrCtn[i].lang,
                link: reviewsArrCtn[i].link,
                date: reviewsArrCtn[i].date,
                revdate: reviewsArrCtn[i].revdate,
                // hometown: fooa
            }
            translate.from = 'el';


            if( ReviewCounterLang === 0){
                let fooa = await translate( reviewsArrCtn[i].hometown , { to: 'en', engine: 'google', key: 'AIzaSyCP6JXuY_k7qGsoSYDZm76m4FFpYv8Q-8Y' }).then(text => {
                    console.log("------")
                    console.log(text);  // Hola mundo

                    rev.hometown = text
                    console.log("------")
                });

                let foob = await translate( reviewsArrCtn[i].date , { to: 'en', engine: 'google', key: 'AIzaSyCP6JXuY_k7qGsoSYDZm76m4FFpYv8Q-8Y' }).then(text => {
                    console.log("------")
                    console.log(text);  // Hola mundo

                    rev.date = text
                    console.log("------")
                });

                let fooc = await translate( reviewsArrCtn[i].revdate , { to: 'en', engine: 'google', key: 'AIzaSyCP6JXuY_k7qGsoSYDZm76m4FFpYv8Q-8Y' }).then(text => {
                    console.log("------")
                    console.log(text);  // Hola mundo

                    rev.revdate = text
                    console.log("------")
                });
            }


            translateFirst.push(rev)

        }

        ReviewCounterLang++

        let RT =  translateFirst.reduce(function(RevArr,obj,index){

            console.log(index)
            let getIn = true;

            if(index === 0){
                //RevArr.push(obj)
            }else{
                for( let i = 0; i < RevArr.length; i++  ){
                    if( RevArr[i].name === obj.name && RevArr[i].heading === obj.heading){
                        getIn = false;
                    }
                }

            }

            console.log(getIn)

            if(getIn){
                RevArr.push(obj)
            }

            return RevArr

        },[])

        fs.writeFile('./static/' + Lang + '.json', JSON.stringify(RT), 'utf8', (err) => {
            console.log("change data output")
            return (err) ? console.log(err) : console.log("The file pages was saved!");
        });

    }

    async function scrap() {

        // const browser = await puppeteer.launch({headless: true , args: ['--no-sandbox', '--disable-setuid-sandbox']} );
        const browser = await puppeteer.launch({headless: false});
        const page = await browser.newPage();
        await page.goto(url , {waitUntil: 'domcontentloaded'} );

        await page.evaluate(() => {
            scroll(0, 99999)
        });
        await page.waitFor(5000);

        let bodyHTMLcheckLangButton = await page.evaluate(() => document.body.innerHTML);

        if ($('.taLnk', bodyHTMLcheckLangButton).length > 0) {
            await page.$$eval('.taLnk', function (elements) {
                elements[0].click()
            });
        }

        let  bodyHTMLcheckLangButton22 = await page.evaluate(() => document.body.innerHTML);
        if ($('#c_ReviewList_Languages input', bodyHTMLcheckLangButton22).length > 0) {

            await page.$$eval('#c_ReviewList_Languages input[value="en"]', function (elements) {
                console.log(21)
                elements[0].click()
            });

        }

        let bodyHTMLStarButton = await page.evaluate(() => document.body.innerHTML);

        if ($('#ReviewRatingFilter_5', bodyHTMLStarButton).length > 0) {
            await page.$$eval('#ReviewRatingFilter_5', function (elements) {
                elements[0].click()
            });
        }

        await page.evaluate(() => {
            scroll(0, 99999)
        });
        await page.waitFor(5000);

        await init(page, scrapCheeriosExcerpt, scrapCheeriosContent, 'firstLang' , 1)

        let bodyHTMLcheckLangButtonSec2 = await page.evaluate(() => document.body.innerHTML);

        if ($('.taLnk', bodyHTMLcheckLangButtonSec2).length > 0) {
            await page.$$eval('.taLnk', function (elements) {
                elements[0].click()
            });
        }

        let bodyHTMLcheckLangButtonSec = await page.evaluate(() => document.body.innerHTML);
        if ($('#c_ReviewList_Languages input', bodyHTMLcheckLangButtonSec).length > 0) {

            await page.$$eval('#c_ReviewList_Languages input[value="el"]', function (elements) {
                elements[0].click()
            });

        }

        await page.evaluate(() => {
            scroll(0, 99999)
        });

        await page.waitFor(5000);
        await init(page, scrapCheeriosExcerpt, scrapCheeriosContent, 'SecondLang' , 2)
        await browser.close()

        done()
    }

    scrap()

}

module.exports = SCRAPDATA
