const PushData = require('./WPDestination/pushData')
let FIRST_LANG = require('./static/booking/firstLang');
let SECOND_LANG = require('./static/booking/SecondLang');

let pushWPData = function(done,username,wpuser,wppass){
    PushData.push(FIRST_LANG,SECOND_LANG,done,username,wpuser,wppass)
}

module.exports = pushWPData