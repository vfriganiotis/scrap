const puppeteer = require('puppeteer'); // v 1.1.0
const $ = require('cheerio');
const fs = require('fs');
const translate = require('translate');

//Atrium
const facebook = 'https://www.facebook.com/AnezinaVillasSantorini'
const hotels = 'https://www.hotels.com/ho329773/'
const booking = 'https://www.booking.com/hotel/gr/anezina-pylos.en-gb.html'
const trip = 'https://www.tripadvisor.com.gr/Hotel_Review-g635608-d589244-Reviews-Anezina_Villas-Perissa_Santorini_Cyclades_South_Aegean.html'


//Stanley
// const facebook = 'https://www.facebook.com/www.hotelstanley.gr/'
// const hotels = 'https://el.hotels.com/ho382988/anezina-villas-santorine-ellada/'
// const booking = 'https://www.booking.com/hotel/gr/stanleyhotelathens.el.html'
// const trip = 'https://www.tripadvisor.com.gr/Hotel_Review-g189400-d233051-Reviews-The_Stanley-Athens_Attica.html'

//Doukissa
// const facebook = 'https://www.google.com'
// const hotels = 'https://www.google.com'
// const booking = 'https://www.booking.com/hotel/gr/doukissa.el.html'
// const trip = 'https://www.tripadvisor.com/Hotel_Review-g642171-d2269885-Reviews-Hotel_Doukissa-Parikia_Paros_Cyclades_South_Aegean.html'

let scores = []


async function scrap() {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.goto(facebook);

    await page.evaluate(() => {
        scroll(0, 99999)
    });
    await page.waitFor(5000);

    let bodyHTMLfacebook = await page.evaluate(() => document.body.innerHTML);

    let facebookData = {
        facebookscore : "",
        facebookpeople : ""
    }
    console.log($('._672g', bodyHTMLfacebook).text())
    console.log($('._2w0b', bodyHTMLfacebook).text())

    facebookData.facebookscore = $('._672g', bodyHTMLfacebook).text()
    let facebookp = $('._2w0b', bodyHTMLfacebook).text().split(' ')
    facebookData.facebookpeople = facebookp[facebookp.length - 2]
    console.log(facebookData.facebookpeople)

    scores.push(facebookData)

    console.log("END fACEBOOK")


    await page.goto(hotels);

    await page.evaluate(() => {
        scroll(0, 99999)
    });
    await page.waitFor(5000);

    let bodyHTMLHotels = await page.evaluate(() => document.body.innerHTML);

    let hotelsData = {
            hotelsscore : "",
            hotelspeople : ""
        }

    console.log($('.total-reviews.trust-you', bodyHTMLHotels).text().replace( /^\D+/g, '').split(' ')[0])
    console.log($('#flexible-container-right-hand-column .rating', bodyHTMLHotels).text())
    console.log("END Hotels")
    hotelsData.hotelspeople = $('.total-reviews.trust-you', bodyHTMLHotels).text().replace( /^\D+/g, '').split(' ')[0];
    hotelsData.hotelsscore = $('#flexible-container-right-hand-column .rating', bodyHTMLHotels).text().replace(/,/g,'.');
    scores.push(hotelsData)
    console.log(hotelsData)

    console.log(scores)


    await page.goto(booking);

    await page.evaluate(() => {
        scroll(0, 99999)
    });
    await page.waitFor(5000);

    let bodyHTMLBooking = await page.evaluate(() => document.body.innerHTML);

    let BookingData = {
            Bookingscore : "",
            Bookingpeople : ""
        }

    console.log($('.hp_nav_reviews_link.toggle_review.track_review_link_zh', bodyHTMLBooking).text().replace( /^\D+/g, '').split(' ')[0].split(')')[0] )
    console.log($('.bui-review-score.c-score.bui-review-score--end .bui-review-score__badge', bodyHTMLBooking).text().trim().replace(/,/g,'.'))

    console.log("END Booking")
    BookingData.Bookingscore = $('.bui-review-score.c-score.bui-review-score--end .bui-review-score__badge', bodyHTMLBooking).text().trim().replace(/,/g,'.')
    BookingData.Bookingpeople = $('.hp_nav_reviews_link.toggle_review.track_review_link_zh', bodyHTMLBooking).text().replace( /^\D+/g, '').split(' ')[0].split(')')[0]

    scores.push(BookingData)
    console.log( scores)


    //
    //
    await page.goto(trip);

    await page.evaluate(() => {
        scroll(0, 99999)
    });
    await page.waitFor(5000);

    let bodyHTMLTrip = await page.evaluate(() => document.body.innerHTML);

    let TripData = {
            Tripscore : "",
            Trippeople : ""
        }

    console.log($('.hotels-hotel-review-about-with-photos-Reviews__overallRating--3cjYf', bodyHTMLTrip).text().replace(/,/g,'.'))
    console.log($('.hotels-hotel-review-about-with-photos-Reviews__seeAllReviews--3jEYF', bodyHTMLTrip).text().split(' ')[0])
    console.log("END Trip")
    TripData.Tripscore = $('.hotels-hotel-review-about-with-photos-Reviews__overallRating--3cjYf', bodyHTMLTrip).text().replace(/,/g,'.')
    TripData.Trippeople = $('.hotels-hotel-review-about-with-photos-Reviews__seeAllReviews--3jEYF', bodyHTMLTrip).text().split(' ')[0]

    scores.push(TripData)

    console.log(scores)

    fs.writeFile('./static/allReviews.json', JSON.stringify(scores) , 'utf8', (err) => {
        console.log("change data output")
        return (err) ? console.log(err) : console.log("The file pages was saved!");
    });

    await browser.close()

}

scrap()



