const fs =require('fs');

const Request = require('./WPrequest')

// let username = 'testwh.sitesdemo.com';

//WP
async function PushData(DATA,SECOND_DATA,done,username,WPusername,WPpassword){

    // let username = 'testwh.sitesdemo.com';

    console.log(WPusername)
    console.log(WPpassword)
    const WPauth = "Basic " + new Buffer(WPusername + ":" + WPpassword).toString("base64");

    let headersAuth = {
        "Authorization" : WPauth,
        "Content-Type" : "application/json",
        "Access-Control-Allow-Origin": "*",
        "Accept": "application/json"
    }

    let WPReviews = 'https://' + username + '/wp-json/wp/v2/reviews';


    console.log(WPReviews)

    for (const item of DATA) {

        const JsonBody = {
            "title" : "Jedi",
            "status" : "publish"
        }

        JsonBody.title = item.heading;
        JsonBody.content = item.content;
        JsonBody.excerpt = item.excerpt;


        JsonBody.fields = {
            link  : item.link,
            name  : item.name,
            langr : item.lang
        }

        const WPCreatePost = {
            url : WPReviews,
            headers : headersAuth,
            method: "POST",
            body: JsonBody,
            json: true
        }

        const v = await Request.init(WPCreatePost)

    }

    for (const item of SECOND_DATA) {

        const JsonBody = {
            "title" : "Jedi",
            "status" : "publish"
        }

        JsonBody.title = item.heading;
        JsonBody.content = item.content;
        JsonBody.excerpt = item.excerpt;

        JsonBody.fields = {
            link : item.link,
            name : item.name,
            langr : item.lang
        }

        const WPCreatePost = {
            url : WPReviews,
            headers : headersAuth,
            method: "POST",
            body: JsonBody,
            json: true
        }

        const v = await Request.init(WPCreatePost)

    }

    console.log('finish')
    done()

}

exports.push = PushData