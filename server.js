const express = require('express');
const app = express();
const path = require('path')
const port = 3003;

const scrapData = require(path.join(__dirname + '/reviews.js'))
const pushData = require(path.join(__dirname + '/push.js'))

app.get('/',function(req, res){

    res.sendFile(path.join(__dirname + '/client/index.html'));

})

app.get('/scrapData',function(req, res){

    function DONE(){
        res.sendFile(path.join(__dirname + '/client/wpfeed.html'));
    }
    scrapData( req.query.URL , DONE )

})

app.get('/pushWPData',function(req, res){

    function DONE(){
        res.sendFile(path.join(__dirname + '/client/finish.html'));
    }
    pushData( DONE, req.query.URL, req.query.USERNAME, req.query.PASSWORD )

})

app.get('/safemode',function(req, res){

    res.sendFile(path.join(__dirname + '/client/wpfeed.html'));

})

const server = app.listen(port);
server.timeout = 0;